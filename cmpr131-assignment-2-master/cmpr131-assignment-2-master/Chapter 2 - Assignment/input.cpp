#include <iostream>
#include "input.h"

namespace input_h
{
	char inputChar(string prompt, string listChars)
	{
		char input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-1: Invalid input. Must be a character type.\n";
			else
			{
				bool bfound = false;
				for (unsigned c = 0; c < listChars.length(); c++)
					if (toupper(listChars[c]) == toupper(input))
					{
						bfound = true;
						break;
					}
				if (!bfound)
				{
					cout << "ERROR-2: Invalid input. Must be a character from the list of '";
					for (unsigned c = 0; c < listChars.length() - 1; c++)
						cout << "'" << static_cast<char>(toupper(listChars[c])) << "', ";
					cout << "or '" << static_cast<char>(toupper(listChars.back())) << "'.\n";
				}
				else
					break;
			}
			cin.clear();
			cin.ignore(999, '\n');

		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input char of y or n
	char inputChar(string prompt, char yes, char no)
	{
		char input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-3: Invalid input. Must be a character type.\n";
			else if (tolower(input) != tolower(yes) && tolower(input) != tolower(no))
				cout << "ERROR-4: Invalid input. Must be a '" << static_cast<char>(toupper(yes)) << "' or '" << static_cast<char>(toupper(no)) << "' character.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input char
	char inputChar(string prompt, bool alphaOrDigit)
	{
		char input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-5: Invalid input. Must be a character type.\n";
			else if (alphaOrDigit && !isalpha(input))
				cout << "ERROR-6: Invalid input. Must be an alphabet character.\n";
			else if (!alphaOrDigit && !isdigit(input))
				cout << "ERROR-7: Invalid input. Must be a digit character.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input char
	char inputChar(string prompt)
	{
		char input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-8: Invalid input. Must be a character type.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input interger
	int inputInteger(string prompt)
	{
		int input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-9: Invalid input. Must be a numeric value.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input integer where type is 1 for only positive, 2 for positive or zero, 1 for only negative or 2 for negatice or zero
	int inputInteger(string prompt, int type)
	{
		int input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-10: Invalid input. Must be a numeric value.\n";
			else if (type == 1 && input <= 0)
				cout << "ERROR-11: Invalid input. Must be a positive value.\n";
			else if (type == 2 && input < 0)
				cout << "ERROR-12: Invalid input. Must be a positive value or zero.\n";
			else if (type == -1 && input >= 0)
				cout << "ERROR-13: Invalid input. Must be a negative value.\n";
			else if (type == -2 && input > 0)
				cout << "ERROR-14: Invalid input. Must be a negative value or zero.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input integer within range, start and end
	int inputInteger(string prompt, int startRange, int endRange)
	{
		int input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-15: Invalid input. Must be a numeric value.\n";
			else if (!(input >= min(startRange, endRange) && input <= max(startRange, endRange)))
				cout << "ERROR-16: Invalid input. Must be from " << startRange << ".." << endRange << ".\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input double
	double inputDouble(string prompt)
	{
		double input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-17: Invalid input. Must be a numeric value.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');

		}
		cin.clear();
		cin.ignore(999, '\n');
		return input;
	}

	//return an input double where type is 1 for only positive, 2 for positive or zero, -1 for only negative or -2 for negatice or zero
	double inputDouble(string prompt, int type)
	{
		double input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-18: Invalid input. Must be a numeric value.\n";
			else if (type == 1 && input <= 0.0)
				cout << "ERROR-19: Invalid input. Must be a positive value.\n";
			else if (type == 2 && input < 0.0)
				cout << "ERROR-20: Invalid input. Must be a positive value or zero.\n";
			else if (type == -1 && input >= 0.0)
				cout << "ERROR-21: Invalid input. Must be a negative value.\n";
			else if (type == -2 && input > 0.0)
				cout << "ERROR-22: Invalid input. Must be a negative value or zero.\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');

		}

		cin.clear();
		cin.ignore(999, '\n');

		return input;
	}

	//return an input double within range, start to end
	double inputDouble(string prompt, double startRange, double endRange)
	{
		double input;
		while (true)
		{
			cout << prompt;
			if (!(cin >> input))
				cout << "ERROR-23: Invalid input. Must be a numeric value.\n";
			else if (!(input >= min(startRange, endRange) && input <= max(startRange, endRange)))
				cout << "ERROR-24: Invalid input. Must be from " << startRange << ".." << endRange << ".\n";
			else
				break;
			cin.clear();
			cin.ignore(999, '\n');
		}
		cin.clear();
		cin.ignore(999, '\n');
		return input;
	}

	string inputString(string prompt, bool getLine)
	{
		string input;
		cout << prompt;
		if (getLine)
			getline(cin, input);
		else
		{
			cin >> input;
			cin.clear();
			cin.ignore(999, '\n');
		}
		return input;
	}
}