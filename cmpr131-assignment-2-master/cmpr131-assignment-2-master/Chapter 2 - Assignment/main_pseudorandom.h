#pragma once
#include <iostream>

namespace pseudorandom
{
	//Precondition: - None
	//Postcondition:- Show the main result page of Pseudorandom Class
	void show_the_main_function();

	//Precondition: - None
	//Postcondition:- Calculate the 1 million of Pseudorandom and output to the Screen
	void test2_main_menu();

	//Precondition: - double arrary of 12 numbers from Pseudorandom after 1 million ran
	//				- int size of the array (12)
	//Postcondition:- Return the approximate of Guassian (with some help from Heather)
	double calculate_guassian_approximate(const double arr[], const int size);

	//Precondition: - double arrary of 12 numbers from Pseudorandom after 1 million ran
	//				- int size of the array (12)
	//Postcondition:- Return the standard deviation from the sample of double array
	double standard_deviation(const double arr[], int size);

	//Precondition: - double arrary of 12 numbers from Pseudorandom after 1 million ran
	//				- int size of the array (12)
	//Postcondition:- Return the mean value from the sample of double array
	double mean(const double arr[], int size);

	//Precondition: - double arrary of 12 numbers from Pseudorandom after 1 million ran
	//				- int size of the array (12)
	//Postcondition:- Return the median value from the sample of double array
	double median(const double arr[], int n);

}