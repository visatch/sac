#pragma once

#include <string>


namespace employee
{
	using namespace std;

	class Employee
	{
	private:
		int id;
		string first_name, last_name, start_date, end_date;
		char status;

	public:
		//Precondition: None
		//Postcondition: Default constructor
		Employee() { id = 0; first_name = ""; last_name = ""; start_date = ""; end_date = ""; status = 'U'; };
		
		//Precondition: int id, string first_name, string last_name, string start_date, string end_date, char status
		//Postcondition: Constructor with all private members
		Employee(int id, string first_name, string last_name, string start_date, string end_date, char status)
		{
			this->id = id;
			this->first_name = first_name;
			this->last_name = last_name;
			this->start_date = start_date;
			this->end_date = end_date;
			this->status = status;
		}
		//Precondition: Employee employee
		//Postcondition: Copy constructor
		void operator = (const Employee employee)
		{
			id = employee.getID();
			last_name = employee.getLast_Name();
			first_name = employee.getFirst_Name();
			status = employee.getStatus();
			start_date = employee.getStart_Date();
			end_date = employee.getEnd_Date();
		}

		//Precondition: None
		//Postcondition: Return id
		int getID() const
		{
			return id;
		}

		//Precondition: int id
		//Postcondition: set id to the input id
		void setID(int id)
		{
			this->id = id;
		}

		//Precondition: None
		//Postcondition: Return first_name
		string getFirst_Name() const
		{
			return first_name;
		}
		
		//Precondition: string name 
		//Postcondition: set name to the first_name
		void setFirst_Name(string name)
		{
			first_name = name;
		}

		//Precondition: None
		//Postcondition: Return last_name
		string getLast_Name() const
		{
			return last_name;
		}

		//Precondition: String name
		//Postcondition: set name to last_name
		void setLast_Name(string name)
		{
			last_name = name;
		}

		//Precondition: None 
		//Postcondition: Return start_date
		string getStart_Date() const
		{
			return start_date;
		}

		//Precondition: string date
		//Postcondition: set start_date to the date
		void setStart_Date(string date)
		{
			start_date = date;
		}

		//Precondition: None
		//Postcondition: Return End_date
		string getEnd_Date() const
		{
			return end_date;
		}

		//Precondition: String date
		//Postcondition: Set end_date to the date
		void setEnd_Date(string date)
		{
			end_date = date;
		}

		//Precondition: None
		//Postcondition: Return Status 
		char getStatus() const
		{
			return status;
		}

		//Precondition: char Status
		//Postcondition: Set the status to the input Status
		void setStatus(char status) 
		{
			this->status = status;
		}
		
	};
}
