// Visa Touch
// 02/24/21	
// Chapter 2 Assignment

#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include <iomanip>
#include "input.h"
#include "List_of_employee.h"
#include "main_pseudorandom.h"

using namespace std;

int main()
{
	do
	{
		system("cls");
		cout << "\n\t\tCMPR131 - CHAPTER 2 ADT Assignment by Visa Touch\n";
		cout << "\t" << string(60, char(205)) << "\n";
		cout << "\t   1> List of Employees\n";
		cout << "\t   2> Pseudorandom\n";
		cout << "\t" << string(60, char(196)) << "\n";
		cout << "\t   0> Exit\n";
		cout << "\t" << string(60, char(205)) << "\n";

		char option = input_h::inputChar("\t   Option: ");
		option = toupper(option);
		switch (static_cast<int>(option))
		{
		case 48: return 0;
		case 49: list_of_employee::showTheListOfEmployeeMenu(); break;
		case 50: pseudorandom::show_the_main_function(); break;
		default: cout << "\n\t   ERROR: Invalid option. Must be from 1..3\n";
		}

		cout << "\n";
		system("pause");
	} while (true);

	return 0;
}