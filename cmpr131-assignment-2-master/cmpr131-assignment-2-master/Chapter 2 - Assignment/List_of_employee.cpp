#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include "input.h"
#include "Employee.h"
#include "List_of_employee.h"

//To Do LIST:
//Testing the update information, should be work by now
namespace list_of_employee
{
	using namespace std;

	void showTheListOfEmployeeMenu()
	{
		
		int size = 100, usedSize = 0;
		//vector<employee::Employee> vec_employees;
		employee::Employee* arr_employee = new employee::Employee[size];
		
		system("cls");
		do
		{
			cout << "\n\t1> List of Employees\n";
			cout << "\t" << string(60, char(205)) << "\n";
			cout << "\t\tA> Read data from file and store into a list\n";
			cout << "\t\tB> Insert a new employee record into the list\n";
			cout << "\t\tC> Update an employee record from the list\n";
			cout << "\t\tD> Display all, active or inactive employee records from the list\n";
			cout << "\t\tE> Write data from the list to a file\n";
			cout << "\t" << string(60, char(196)) << "\n";
			cout << "\t\t0> Return\n";
			cout << "\t" << string(60, char(205)) << "\n";

			char option = toupper(input_h::inputChar("\t\tOption: "));
			switch (static_cast<int>(option))
			{
			case 48: delete[] arr_employee; return;
			case 65: read_data_from_file(arr_employee, usedSize, size); break;
			case 66: insert_a_new_exployee(arr_employee, usedSize, size); break;
			case 67: update_the_employee_records(arr_employee, usedSize, size); break;
			case 68: showDisplayMenu(arr_employee, usedSize, size); break;
			case 69: write_data_from_the_list_to_a_file(arr_employee, usedSize, size);break;; break;
			default: cout << "\n\t   ERROR: Invalid option. Must be from A,B,C,D,E,0\n";

			}
			cout << "\n";
		} while (true);

	}

	void read_data_from_file(employee::Employee* arr, int &usedSize, int &size)
	{
		string file_name = input_h::inputString("\n\t\tEnter the file name: ",false);

		ifstream fin(file_name);

		if (fin.is_open())
		{
			int id = 0;
			if (usedSize > 0)
				id = arr[usedSize-1].getID();

			string input; char read_char;
			string str_input[6] = {};
			int i = 0;
			while (fin.get(read_char))
			{
				if (read_char != '\n' && read_char != ',')
					input += read_char;
				else
				{
					//tmp_vec.push_back(input);
					str_input[i] = input;
					i++;
					input.clear();

					if (read_char == '\n')
					{
						if (usedSize >= size)
						{
							cout << "\n\t\tError: List is full.\n";
							return;
						}

						id++;
						employee::Employee tmp(id, str_input[3], str_input[2], str_input[4], str_input[5], str_input[0][0]);
						arr[usedSize] = tmp;
						usedSize++;
						i = 0;
					}
				}
			}
			fin.close();
			cout << "\n\t\tCompleted transfering data from the file to list.";
		}
		else
		{
			cout << "\n\t\tError While reading the data file.\n";
		}	
	}

	void insert_a_new_exployee(employee::Employee* arr, int& usedSize, int& size)
	{
		int id = 1;
		if (usedSize > 0)
			id = (arr[usedSize-1].getID()) + 1;
		
		string last_name = input_h::inputString("\n\t\tEnter the last name: ", false);
		string first_name = input_h::inputString("\t\tEnter the first name: ", false);
		string start_date = inputStartDate();

		if (usedSize + 1 >= size)
		{
			cout << "\n\t\tError: The list is full.\n";
			return;
		}

		arr[usedSize] = employee::Employee(id, first_name, last_name, start_date, "current", 'A');
		usedSize++;
	
		//vec.push_back(employee::Employee(id, first_name, last_name, start_date, "current", 'A'));

		cout << "\n\t\tCompleted adding a new record.\n";
	}

	bool isLeapYear(int year)
	{
		return (year % 400 == 0) || ((year % 4 == 0) & (year % 100 != 0));
	}

	string inputStartDate()
	{
		string start_date;

		do
		{
			start_date = input_h::inputString("\t\tEnter the starting date: ", false);
			if (start_date.size() == 10)
			{
				if (start_date.at(2) == '/' || start_date.at(5) == '/')
				{
					int month = stoi(start_date.substr(0,2));
					int day = stoi(start_date.substr(3, 2));
					int year = stoi(start_date.substr(6, 4));

					if (!validateInputDate(month,day,year))
					{
						cout << "\t\tERROR: Invalid date input. Must be a mm/dd/yyyy.\n";
						continue;
					}
					else	
						break;
				}
			}
			cout << "\t\tERROR: Invalid date input. Must be a mm/dd/yyyy.\n";
			
		} while (true);

		return start_date;
	}

	bool validateInputDate(int month, int day, int year)
	{
		if (year >= 1960 && year <= 2021 && month <= 12 && day <= 31)
		{
			if ((month == 2 && !isLeapYear(year) && day >= 29) || (month == 2 && day >= 30) || (month == 4 || month == 6 || month == 9 || month == 11 && day == 31))
				return false;
			else
				return true;
		}
		return false;
	}
	// 1 3 5 7 8 10 12
	// 2 4 6 9 11

	void update_the_employee_records(employee::Employee* arr, int& usedSize, int& size)
	{
		if (usedSize == 0)
		{
			cout << "\n\t\tERROR: list is empty.\n";
			return;
		}

		string prompt = "\n\t\tEnter a valid employee ID (1.." + to_string(usedSize) + string(1,')') + ": ";
		int selected_id = input_h::inputInteger(prompt, 1);
		employee::Employee tmp_emp(arr[selected_id- static_cast<int>(1)]);

		do
		{
			cout << "\n\t\tUpdate Employee ID: " << selected_id << " Record Information\n";
			cout << "\t\t" << string(45, char(205)) << "\n";
			cout << "\t\t" << setw(25) << left << "A> Current status" << ": " << tmp_emp.getStatus()
				<< (tmp_emp.getStatus() == 'A' ? "(Active)" : (tmp_emp.getStatus() == 'I') ? "(Inactive)" : (tmp_emp.getStatus() == 'U') ? "(Unknown)" : "") << "\n";
			cout << "\t\t" << setw(25) << left << "B> Current last name" << ": " << tmp_emp.getLast_Name() << "\n";
			cout << "\t\t" << setw(25) << left << "C> Current firstname name" << ": " << tmp_emp.getFirst_Name() << "\n";
			cout << "\t\t" << setw(25) << left << "D> Current Starting date" << ": " << tmp_emp.getStart_Date() << "\n";
			cout << "\t\t" << setw(25) << left << "E> Current Ending date" << ": " << tmp_emp.getEnd_Date() << "\n";
			cout << "\t\t" << string(45, char(196)) << "\n";
			cout << "\t\t1> Commit the change(s) and return\n";
			cout << "\t\t0> Uncommit the change(s) and return\n";
			cout << "\t\t" << string(45, char(205)) << "\n";

			char option = toupper(input_h::inputChar("\t\tOption: "));
			switch (static_cast<int>(option))
			{
			case 48: return;
			case 49: 
				arr[selected_id - static_cast<int>(1)] = tmp_emp;
				cout << "\n\t\tUpdated the employee record.";
				return;
			case 65: 
				tmp_emp.setStatus(toupper(input_h::inputChar("\n\t\tChange status to A-active, I-inactice or U-Unknown status: ", string("aiu")))); 
				break;
			case 66: 
				tmp_emp.setLast_Name(input_h::inputString("\n\t\tEnter the new last name: ",false)); 
				break;
			case 67: 
				tmp_emp.setFirst_Name(input_h::inputString("\n\t\tEnter the new first name: ",false)); 
				break;
			case 68: 
				cout << '\n'; tmp_emp.setStart_Date(inputStartDate()); 
				break;
			case 69: 
				cout << '\n'; tmp_emp.setEnd_Date(inputStartDate()); 
				break;
			default: 
				cout << "\n\t   ERROR: Invalid option. Must be from A,B,C,D,E,0\n";
			}

			cout << "\n";
		} while (true);
	}

	void showDisplayMenu(employee::Employee* arr, int& usedSize, int& size)
	{
		if (usedSize <= 0)
		{
			cout << "\n\t   ERROR: list is empty.\n";
			return;
		}

		do
		{
			cout << "\n\t\tDisplay Employee Record Information\n";
			cout << "\t\t" << string(45, char(205)) << "\n";
			cout << "\t\t   A> All records\n";
			cout << "\t\t   B> Active records\n";
			cout << "\t\t   C> Inactive records\n";
			cout << "\t\t" << string(45, char(196)) << "\n";
			cout << "\t\t   0> Return\n";
			cout << "\t\t" << string(45, char(205)) << "\n";

			char option = toupper(input_h::inputChar("\t\t   Option: "));
			switch (static_cast<int>(option))
			{
			case 48: return;
			case 65: output_all_employees(arr, usedSize,'A'); break;
			case 66: output_all_employees(arr, usedSize, 'B'); break;
			case 67: output_all_employees(arr, usedSize, 'C'); break;
			default: cout << "\n\t\t   ERROR: Invalid option. Must be from A,B,C,0\n";
			}

			cout << "\n";
		} while (true);
	}

	void output_all_employees(employee::Employee* arr, int& usedSize, char type)
	{
		cout << "\n";
		bool isRecordExist = false;
		for (int i = 0; i < usedSize; i++)
		{
			if (type == 'A')
			{ 
				output_an_object_of_employee(arr[i]);
				isRecordExist = true;
			}
			else if (type == 'B')
			{
				if (arr[i].getStatus() == 'A')
				{
					output_an_object_of_employee(arr[i]);
					isRecordExist = true;
				}
			}
			else if (type == 'C')
				if (arr[i].getStatus() == 'I')
				{
					output_an_object_of_employee(arr[i]);
					isRecordExist = true;
				}
		}

		if (!isRecordExist)
			cout << "\n\t\tNo record found.\n";
	}

	void write_data_from_the_list_to_a_file(employee::Employee* arr, int& usedSize, int& size)
	{
		string file_name = input_h::inputString("\n\t\tEnter a file name: ", false);

		ofstream fout(file_name);
		
		if (fout.is_open())
		{
			for (int i = 0; i < usedSize; i++)
			{
				fout << arr[i].getStatus() << "," << arr[i].getID() << "," << arr[i].getLast_Name()
					<< "," << arr[i].getFirst_Name() << "," << arr[i].getStart_Date() << "," << arr[i].getEnd_Date() << "\n";

			}
			fout.close();
			cout << "\n\t\tCompleted writing to the file, " << file_name << ".";
		}
		else
			cout << "\n\t\tError: While writing from the list to file.\n";
	}

	void output_an_object_of_employee(employee::Employee emp)
	{
		cout << "\t\t\t" << setw(15) << left << "Employee ID" << ": " << emp.getID() << "\n";
		cout << "\t\t\t" << setw(15) << left << "Name" << ": " << emp.getLast_Name() << ", " << emp.getFirst_Name() << "\n";
		cout << "\t\t\t" << setw(15) << left << "Status" << ": " << emp.getStatus() << "\n";
		cout << "\t\t\t" << setw(15) << left << "Start Date" << ": " << emp.getStart_Date() << "\n";
		cout << "\t\t\t" << setw(15) << left << "End Date" << ": " << emp.getEnd_Date() << "\n\n";
	}
}