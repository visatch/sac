#pragma once
#include <iostream>

class Pseudorandom
{
private:
	int multiplier, seed, increment, modulus;

public:
	//Precondition: None 
	//Postcondition: Default Constructor
	Pseudorandom()
	{
		multiplier = 40;
		increment = 725;
		modulus = 729;
		seed = 1;
	}
	//Precondition: None 
	//Postcondition: Constructor with all private members
	Pseudorandom(int multiplier, int seed, int increment, int modulus) 
	{
		this->multiplier = multiplier;
		this->seed = seed;
		this->increment = increment;
		this->modulus = modulus;
	}
	//Precondition: None
	//Postcondition: Return increment
	int getIncrement() const
	{
		return increment;
	}
	//Precondition: int increment
	//Postcondition: set the input increment to the increment
	void setIncrement(int increment)
	{
		this->increment = increment;
	}
	//Precondition: None
	//Postcondition: Return multiplier 
	int getMultiplier() const
	{
		return multiplier;
	}
	//Precondition: int multiplier
	//Postcondition: set the input muliplier to the multiplier
	void setMultiplier(int multiplier)
	{
		this->multiplier = multiplier;
	}
	//Precondition: None
	//Postcondition: set the input seed to the seed
	void setSeed(int seed)
	{
		this->seed = seed;
	}
	//Precondition: None 
	//Postcondition: Return seed
	int getSeed() const
	{
		return seed;
	}
	//Precondition: int modulus
	//Postcondition: set the input modulus to the modulus
	void setModoulus(int modulus)
	{
		this->modulus = modulus;
	}
	//Precondition: None 
	//Postcondition: Return modulus
	int getModulus() const
	{
		return modulus;
	}
	//Precondition: None
	//Postcondition: Calculate the next seed number and 
	//						update the seed to the caulcuated seed number
	int calculate_seed()
	{
		seed = (multiplier * seed + increment) % modulus;
		this->seed = seed;
		return seed;
	}
	//Precondition: None
	//Postcondition: Return the exact value of the pseudorandom
	double calculate_seed_return_divided_number()
	{
		double tmp = static_cast<double>(calculate_seed());
		return tmp / static_cast<double>(modulus);
	}

	

};
