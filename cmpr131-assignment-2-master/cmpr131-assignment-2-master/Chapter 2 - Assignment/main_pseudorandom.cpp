#include "Pseudorandom.h"
#include "main_pseudorandom.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <string>

namespace pseudorandom
{
	using namespace std;

	void show_the_main_function()
	{
		cout << "\n\t2> Pseudorandom Project\n";
		cout << "\n\ttest1 (pseudorandom): \n\t\tSeed = 1\n";

		Pseudorandom pseudorandom;

		vector<int> vec_random_num = {};
		for (int i = 0; i < pseudorandom.getModulus(); i++)
			vec_random_num.push_back(pseudorandom.calculate_seed());

		sort(vec_random_num.begin(), vec_random_num.end());


		//-2 scores
		cout << "\n\t\tGenerated random " << pseudorandom.getModulus() << " different numbers.\n";

		test2_main_menu();

	}

	void test2_main_menu()
	{
		cout << "\n\ttest2 (pseudorandom): \n";

		srand(static_cast<unsigned int>(time(NULL)));
		int multiplier = rand() % 10000, increment = rand() % 30000, modulus = rand() % 30000, seed = 1;

		cout << "\n\t\tMultiplier = " << multiplier << ", increment = " << increment << ", modulus = " << modulus << ", the new seed = " << seed;

		//Pseudorandom pseudo(7066, seed, 8164, 30676);
		Pseudorandom pseudo(multiplier, seed, increment, modulus);

		int count_list[10] = { 0 };

		for (int i = 0; i < 1000000; i++)
		{
			double tmp = (pseudo.calculate_seed_return_divided_number());
			if (tmp >= 0 && tmp < 0.1)
				count_list[0] += 1;
			else if (tmp >= 0.1 && tmp < 0.2)
				count_list[1] += 1;
			else if (tmp >= 0.2 && tmp < 0.3)
				count_list[2] += 1;
			else if (tmp >= 0.3 && tmp < 0.4)
				count_list[3] += 1;
			else if (tmp >= 0.4 && tmp < 0.5)
				count_list[4] += 1;
			else if (tmp >= 0.5 && tmp < 0.6)
				count_list[5] += 1;
			else if (tmp >= 0.6 && tmp < 0.7)
				count_list[6] += 1;
			else if (tmp >= 0.7 && tmp < 0.8)
				count_list[7] += 1;
			else if (tmp >= 0.8 && tmp < 0.9)
				count_list[8] += 1;
			else if (tmp >= 0.9 && tmp < 1.0)
				count_list[9] += 1;
		}

		const int size = 12; double new_arr_12[size] = { 0 };

		for (int i = 0; i < 12; i++)
			new_arr_12[i] = pseudo.calculate_seed_return_divided_number();

		cout << "\n\n\t\tRange\t\tNumber of Occurences\n";
		for (int i = 0; i < 10; i++)
			cout << "\t\t[0." << i << (i < 9 ? " ... 0." + to_string(i + 1) + ")" : " ... 1.0)") << "\t" << count_list[i] << "\n";

		cout << "\n\t\tWith 12 uniformly distributed rand number in the range[0...1.0),\n\t\tthe approximate Gaussian distribution is "
			<< calculate_guassian_approximate(new_arr_12, size) << ".\n";
	}

	double calculate_guassian_approximate(const double arr[], const int size)
	{
		double tmp = 0, sum_of_the_12_nums = 0;
		for (int i = 0; i < size; i++)
		{
			sum_of_the_12_nums += arr[i];
		}

		double median_val = median(arr, size), mean_val = mean(arr, size), sd_val = standard_deviation(arr, size);

		//Formula to calculate Gaussian 
		return (median_val + (sum_of_the_12_nums - 6) * sd_val);
	}

	double mean(const double arr[], int size)
	{
		double result = 0;
		for (int i=0;i<size;i++)
			result += arr[i];
		return result / static_cast<double>(size);
	}

	double standard_deviation(const double arr[], int size)
	{
		double result = 0;

		if (size == 0)
			return result;

		double mean_val = mean(arr,size);

		for (int i = 0; i < size; i++)
		{
			double tmp = (static_cast<double>(arr[i]) - mean_val);
			result += tmp * tmp;
		}

		result = sqrt(result / (size - 1.0));

		return result;
	}

	double median(const double arr[], int n)
	{
		return (n % 2 == 0 ? ((arr[(static_cast<unsigned int>(n / 2 - 1))] + 
			static_cast<double>(arr[static_cast<unsigned int>(n / 2)])) / 2.0) : arr[(static_cast<unsigned int>((n + 1) / 2) - 1)]);
	}

	
	
	

	


}