#pragma once

#ifndef INPUT_H
#define INPUT_H

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

namespace input_h
{
	char inputChar(string prompt, string listChars);
	char inputChar(string prompt, char yes, char no);
	char inputChar(string prompt, bool alphaOrDigit);
	char inputChar(string prompt);
	int inputInteger(string prompt);
	int inputInteger(string prompt, int type);
	int inputInteger(string prompt, int startRange, int endRange);
	double inputDouble(string prompt);
	double inputDouble(string prompt, int type);
	double inputDouble(string prompt, double startRange, double endRange);
	string inputString(string prompt, bool getLine);
}

#endif // !INPUT_H