#pragma once
#include "Employee.h"
#include <vector>

namespace list_of_employee
{ 
	////Precondition: None
	//Postcondition: Show the menu to manage the Employee records
	void showTheListOfEmployeeMenu();
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Read the data from the file and put to the list 
	void read_data_from_file(employee::Employee* arr, int &usedSize ,int &size);
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Show the menu to display the Employee records 
	void showDisplayMenu(employee::Employee* arr, int& usedSize, int& size);
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Update the all field on the Employee record
	void update_the_employee_records(employee::Employee* arr, int& usedSize, int& size);
	
	//Precondition: Char: 'A'-All,'B'-Only Active, 'C'-Inactive
	//				- Employee* arr, int usedSize, int Size
	//Postcondition: Display the Employee record
	void output_all_employees(employee::Employee* arr, int& usedSize, char type);
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Display the each Employee record
	void output_an_object_of_employee(employee::Employee emp);
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Insert a new employee record to the list
	void insert_a_new_exployee(employee::Employee* arr, int& usedSize, int& size);
	//Precondition: int year
	//Postcondition: Return true if the input year is leap year, otherwise
	bool isLeapYear(int year);
	//Precondition: None
	//Postcondition: Validate the input start date if it is legit format (mm/dd/yyyy)
	string inputStartDate();
	//Precondition: int month, day, year
	//Postcondition: validate the input date (1960-current year only)
	bool validateInputDate(int month, int day, int year);
	//Precondition: Employee* arr, int usedSize, int Size
	//Postcondition: Write the Employee records from the list to the file
	void write_data_from_the_list_to_a_file(employee::Employee* arr, int& usedSize, int& size);

}
