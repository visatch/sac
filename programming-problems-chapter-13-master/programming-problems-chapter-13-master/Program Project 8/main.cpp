// Name: Visa Touch
// Date: 10-20-2020
// Description: Chapter13 - Programming Project 8

#include <iostream>
#include "input.h"
#include "Queue.h"
#include <time.h>

using namespace queueVisa;

void customerArrive(Queue& queue, int& index);
void helpCustomer(Queue& queue, int index, long& waitTime);

int main()
{
	cout << "Programming Project 8\n";
	Queue DMVqueue;
	int ans = -1, index = 0l; long waitTime = 0;
	cout << "\nThe line is empty.\n";
	do
	{
		ans = inputInteger("Enter '1' to simulate a customer's arrival, '2' to help the customer, or '3' to quit.\n");
		switch (ans)
		{
		case 1:
			customerArrive(DMVqueue, index);
			break;
		case 2:
			helpCustomer(DMVqueue, index, waitTime);
			break;
		case 3:
			break;
		}
	} while (ans != 3);
	cout << "\nQueue ended.\n";
	

	return 0;
}

void customerArrive(Queue& queue, int& index)
{
	index += 1;
	long enteredTime = static_cast<long>(time(NULL));
	queue.add(index, enteredTime);
	cout << "Customer " << index << " entered the queue at time " << enteredTime << ".\n\n" ;
}

void helpCustomer(Queue& queue, int index, long& waitTime)
{
	if (!queue.empty())
	{
		long endTime = static_cast<long>(time(NULL));
		long queueTime = endTime - queue.getTime();
		waitTime = waitTime + queueTime;
		int tmp = queue.remove();
		cout << "Customer " << tmp << " is being helped at time " << endTime << ". Wait time = " << queueTime << " seconds.\n";
		if (!queue.empty())
			cout << "The estimated wait time for customer " << tmp + 1 << " is " << waitTime / tmp << " seconds.\n";
	}
	else
		cout << "Error: Queue is empty.\n";
	cout << "\n";
}    


