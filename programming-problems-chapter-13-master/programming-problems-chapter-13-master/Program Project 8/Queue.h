#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>

namespace queueVisa
{
	using namespace std;
	struct QueueNode
	{
		int data;
		long enteredTime;
		QueueNode* link;
	};

	typedef QueueNode* QueueNodePtr;

	class Queue
	{
	private:
		QueueNodePtr front;
		QueueNodePtr back;
	public:
		//Precondition: None
		//Postcondition: Create a default object
		Queue();
		//Precondition: Queue& = queue
		//Postcondition: Create a queue from another queue 
		Queue(const Queue& queue);
		//Destructor
		~Queue();
		//Precondition: char item
		//Postcondition: add an item to back of the queue
		void add(int item, long time);
		//Precondition: The queue is not empty
		//Postcondition: Return the item at the front of the queue and removes that item from the queue
		int remove();
		//Precondition: The queue is not empty
		//Postcondition: Return the data that removed from the queue | return -1 when failed
		bool empty() const;
		//Precondition: None
		//Postcondition: Returns time with first QueueNode of the queue
		long getTime() const;

	};
}
#endif


