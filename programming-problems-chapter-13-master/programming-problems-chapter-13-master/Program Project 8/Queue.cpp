#include "Queue.h"
#include <iostream>

namespace queueVisa
{	
	using namespace std;
	Queue::Queue() :front(NULL), back(NULL) {};
	
	Queue::Queue(const Queue& queue)
	{
		if (queue.empty())
			front = back = NULL;
		else
		{
			QueueNodePtr tmpPtrOld = queue.front;
			QueueNodePtr tmpPtrNew;
			back = new QueueNode;
			back->data = tmpPtrOld->data;
			back->enteredTime = tmpPtrOld->enteredTime;
			back->link = NULL;
			front = back;
			tmpPtrOld = tmpPtrOld->link;
			while (tmpPtrOld != NULL)
			{
				tmpPtrNew = new QueueNode;
				tmpPtrNew->data = tmpPtrOld->data;
				tmpPtrNew->enteredTime = tmpPtrOld->enteredTime;
				tmpPtrNew->link = NULL;
				back->link = tmpPtrNew;
				back = tmpPtrNew;
				tmpPtrOld = tmpPtrNew->link;
			}
		}
	};

	Queue:: ~Queue()
	{
		int tmp;
		while (!empty())
			tmp = remove();
	}

	void Queue::add(int item, long time)
	{
		if (empty())
		{
			front = new QueueNode;
			front->data = item;
			front->enteredTime = time;
			front->link = NULL;
			back = front;
		}
		else
		{
			QueueNodePtr tmpPtr;
			tmpPtr = new QueueNode;
			tmpPtr->data = item;
			tmpPtr->enteredTime = time;
			tmpPtr->link = NULL;
			back->link = tmpPtr;
			back = tmpPtr;
		}
	}

	int Queue::remove()
	{
		if (empty())
		{
			cout << "Error: Removing an item from an empty queue.\n";
			return -1;
		}
		char result = front->data;
		QueueNodePtr discard;
		discard = front;
		front = front->link;
		if (front == NULL)
			back = NULL;
		
		delete discard;
		return result;
	}

	bool Queue::empty() const
	{
		return (back == NULL);
	}
	long Queue::getTime() const
	{
		return front->enteredTime;
	}
}