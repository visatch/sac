#ifndef Node_h
#define Node_h

#include <iostream>
#include <string>

namespace NodeNameSpace
{
	using namespace std;

	class Node
	{
	private:
		int num;
		Node* link;

	public:
		//Precondition: None
		//Postcondition: Create an object with empty num and link = NULL
		Node();
		//Precondition: value of integer num(inputNum)
		//Postcondition: Create an object with member object name = value and link = NULL
		Node(int inputNum);
		//Precondition: value of integer num = inputNum, Pointe of Node = next
		//Postcondition: Create an object with member object name = inputName and link = next
		Node(int inputNum, Node* next);
		//Precondition: value of string = inputNum
		//Postcondition: set member object num = inputNum
		void setNum(int inputNum);
		//Precondition: None
		//Postcondition: return the value of member var. num
		int getNum() const;
		//Precondition: None 
		//Postcondition: return the value of member var link as pointer
		Node* getLink() const;
		//Precondition: pointer of class Node = next
		//Postcondition: set the member var link = next
		void setLink(Node* next);
	};
	typedef Node* NodePtr;
}
#endif // !Node_h
