// Name: Visa Touch
// Date: 10-20-2020
// Description: Chapter13 - Programming Project 1

#include <iostream>
#include "Node.h"

using namespace std;
using namespace NodeNameSpace;

//PreCondition: NodePtr& head, String value
//PostCondition: insert a new node with value of String to the head of previous head
void headinsert(NodePtr& head, int num);

//Precondition: Linkedlist head
//Postcondition: reverse the linked list 
void reverseLinkedList(NodePtr& head);

//PreCondition: None
//PostCondition: output all the name in the list
void outputAll(NodePtr& head);

//PreCondition: NodePtr& head
//PostCondition: delete the node which matches with the string value 
void deleteAll(NodePtr& head);

int main()
{
	cout << "Programming Project 1\n";
	NodePtr listPtr = new Node();
	headinsert(listPtr, 1);
	headinsert(listPtr, 2);
	headinsert(listPtr, 3);
	cout << "\nOutput Before Reverse the list:\n";
	outputAll(listPtr);

	//for (int i = 1; i < 100; i++)
	//	headinsert(listPtr, i);
	cout << "\nOutput After Reverse the list:\n";
	reverseLinkedList(listPtr);
	outputAll(listPtr);
	return 0;
}

void reverseLinkedList(NodePtr& head)
{
	cout << "\nReversing the linked list:....\n";
	if (head->getLink() == NULL)
		return ;
	NodePtr currentNode = head, previousNode = NULL, next = NULL;
	while (currentNode != NULL)
	{
		next = currentNode->getLink();
		currentNode->setLink(previousNode);

		previousNode = currentNode;
		currentNode = next;
	}
	head = previousNode;
	cout << "Reversed Linked List completed.\n";
}

void outputAll(NodePtr& head)
{
	NodePtr currentPtr = head;

	while (currentPtr != NULL)
	{
		cout << currentPtr->getNum() << "\n";
		currentPtr = currentPtr->getLink();
	}
	cout << "Output completed\n";
}

void headinsert(NodePtr& head, int value)
{
	NodePtr tmpPtr;
	tmpPtr = new Node(value, head);
	head = tmpPtr;
}

void deleteAll(NodePtr& head)
{
	NodePtr currentPtr = head, previousPtr = currentPtr;
	while (currentPtr != NULL)
	{
		previousPtr = currentPtr;
		currentPtr = currentPtr->getLink();
		delete previousPtr;
	}
	cout << "Delete all nodes sucessfully.\n";
}