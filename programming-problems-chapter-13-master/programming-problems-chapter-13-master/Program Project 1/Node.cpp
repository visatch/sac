#include "Node.h"
#include <iostream>
#include <string>

namespace NodeNameSpace
{
	Node::Node() :num(), link(NULL) {}

	Node::Node(int inputNum) : num(inputNum), link(NULL) {}

	Node::Node(int inputNum, Node* next) : num(inputNum), link(next) {};

	void Node::setNum(int inputNum)
	{
		num = inputNum;
	}

	int Node::getNum() const 
	{
		return num;
	}

	Node* Node::getLink() const
	{
		return link;
	}

	void Node::setLink(Node* next)
	{
		link = next;
	}

}
