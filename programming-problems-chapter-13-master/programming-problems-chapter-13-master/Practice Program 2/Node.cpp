#include "Node.h"
#include <iostream>
#include <string>

namespace classofNode
{
	Node::Node() :name(), link(NULL) {}

	Node::Node(string inputName) : name(inputName), link(NULL) {}

	Node::Node(string inputName, Node* next) : name(inputName), link(next) {};

	void Node::setName(string inputName)
	{
		name = inputName;
	}

	string Node::getName() const
	{
		return name;
	}

	Node* Node::getLink() const
	{
		return link;
	}

	void Node::setLink(Node* next)
	{
		link = next;
	}
}