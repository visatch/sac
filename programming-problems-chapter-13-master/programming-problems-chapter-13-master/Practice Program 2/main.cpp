// Name: Visa Touch	
// Date: 10-28-2020
// Description: Chapter13 - Practice Program 2

#include <iostream>
#include <string>
#include "Node.h"

using namespace classofNode;
using namespace std;

//PreCondition: NodePtr& head, String value
//PostCondition: insert a new node with value of String to the head of previous head
void headinsert(NodePtr& head, string value);

//PreCondition: None
//PostCondition: output all the name in the list
void outputAll(NodePtr& head);

//PreCondition: String value and String after
//PostCondition: sucess -> return value of string value, fail -> return empty
void insertAfter(NodePtr& head, string value, string after);

//PreCondition: NodePtr& head and String valueToDelete
//PostCondition: delete the node which matches with the string value 
void deleteOneNode(NodePtr& head, string valueToDelete);

//PreCondition: NodePtr& head
//PostCondition: delete the node which matches with the string value 
void deleteAll(NodePtr& head);


int main()
  {
	cout << "Practice Program 2\n";
	NodePtr listPtr;
	listPtr = new Node("Emily");
	headinsert(listPtr, "James");
	headinsert(listPtr, "Joules");
	
	cout << "A. Output in order all names in the list";
	outputAll(listPtr);

	cout << "\nB. Insert the name \"Joshua\" after \"James\"\n";
	insertAfter(listPtr, "Joshua", "James");
	outputAll(listPtr);

	cout << "C. Delete the node with \"Joules\"";
	deleteOneNode(listPtr, "Joules");
 	outputAll(listPtr);

	cout << "\nD. Delete all nodes\n";
	deleteAll(listPtr);


	return 0;
}


void headinsert(NodePtr& head, string value)
{
	NodePtr tmpPtr;
	tmpPtr = new Node(value,head);
	head = tmpPtr;
}

void outputAll(NodePtr& head)
{
	cout << "\nOutputs in order all names in the list:\n";
	NodePtr currentPtr = head;

	while (currentPtr != NULL)
	{
		cout << currentPtr->getName() << "\n";
		currentPtr = currentPtr->getLink();
	}
}

//I should check first and abort if there is no string after in the list
void insertAfter(NodePtr& head, string value, string after)
{
	NodePtr tmp, listPtr;
	tmp = new Node;
	tmp->setName(value);

	listPtr = head;
	while (listPtr != NULL && listPtr->getName() != after )
	{
		listPtr = listPtr->getLink();
	}

	if (listPtr != NULL)
	{
		tmp->setLink(listPtr->getLink());
		listPtr->setLink(tmp);
	}
	
}

void deleteOneNode(NodePtr& head, string valueToDelete)
{
	if (head == NULL)
		return;

	if (valueToDelete == head->getName())
	{
		NodePtr tmp = head;
		head = head->getLink();
		delete tmp;
		return;
	}

	NodePtr currentPtr = head, previousPtr = currentPtr;
	while (currentPtr != NULL && currentPtr->getName() != valueToDelete)
	{
		previousPtr = currentPtr;
		currentPtr = currentPtr->getLink();
	}
	if (currentPtr != NULL)
	{
		previousPtr->setLink(currentPtr->getLink());
		currentPtr = currentPtr->getLink();
		delete previousPtr;
	}
}

void deleteAll(NodePtr& head)
{
	NodePtr currentPtr = head, previousPtr = currentPtr;
	while (currentPtr != NULL)
	{
		previousPtr = currentPtr;
		currentPtr = currentPtr->getLink();
		delete previousPtr;
	}
	cout << "Delete all nodes sucessfully.\n";
}


