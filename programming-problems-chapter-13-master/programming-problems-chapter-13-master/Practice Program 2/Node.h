#ifndef Node_h
#define Node_h

#include <iostream>
#include <string>

using namespace std;

namespace classofNode
{
	class Node
	{
	private:
		string name;
		Node* link;
	public:
		//Precondition: None
		//Postcondition: Create an object with empty name and link = NULL
		Node();
		//Precondition: value of string name(inputName)
		//Postcondition: Create an object with member object name = value and link = NULL
		Node(string inputName);
		//Precondition: value of string name = inputName, Pointe of Node = next
		//Postcondition: Create an object with member object name = inputName and link = next
		Node(string inputName, Node* next);
		//Precondition: value of string = inputName
		//Postcondition: set member object name = inputName
		void setName(string inputName);
		//Precondition: None
		//Postcondition: return the value of member var name
		string getName() const;
		//Precondition: None 
		//Postcondition: return the value of member var link as pointer
		Node* getLink() const;
		//Precondition: pointer of class Node = next
		//Postcondition: set the member var link = next
		void setLink(Node* next);
	};
	typedef Node* NodePtr;
}

#endif // !Node.h