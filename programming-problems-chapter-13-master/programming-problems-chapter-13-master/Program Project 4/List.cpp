#include "List.h"

namespace listVisa
{
	List::List() : head(NULL), size (0) {};

	void List::addItem(double item)
	{
		if (!full())
		{
			NodePtr tmpPtr = new Node;
			tmpPtr->data = item;
			tmpPtr->link = head;
			head = tmpPtr;
			size += 1;
		}
		else
			cout << "List is full.\n";
	}

	bool List::full()
	{
		return (size == MAX_LIST_SIZE);
	}

	void List::removeLast()
	{
		if (head == NULL)
			return;

		if (head->link == NULL)
		{
			delete head;
			head = nullptr;
			size--;
			return;
		}

		NodePtr secondPrevious = head;
		while (secondPrevious->link->link != NULL)
		{
			secondPrevious = secondPrevious->link;
		}
		NodePtr tmp = secondPrevious->link;
		secondPrevious->link = NULL;
		delete tmp;
		size--;
		cout << "Remove last item from the list sucessfully.\n";
	}

	double List::getLast()
	{
		NodePtr tmp = head;
		if (tmp == NULL)
			return -1;

		while (tmp != NULL)
		{
			if (tmp->link == NULL)
				return tmp->data;
			tmp = tmp->link;
		}

		return -1;
	}

	ostream& operator <<(ostream& out, const List& list)
	{
		NodePtr tmp = list.head;
		if (list.size > 0)
			cout << "\nOutput the list:\n";
		while (tmp != NULL)
		{
			cout << tmp->data << '\n';
			tmp = tmp->link;
		}
		return out;
	}

	List::~List()
	{
		NodePtr currentPtr = head, previousPtr = currentPtr;
		while (currentPtr != NULL)
		{
			previousPtr = currentPtr;
			currentPtr = currentPtr->link;
			delete previousPtr;
		}
	}
	double List::getDataAtPos(int pos)
	{
		if (head == NULL)
			return - 1;
		int index = 0;
		NodePtr tmp = head;
		while (tmp != NULL)
		{
			if (index == pos-1)
				return tmp->data;
			tmp = tmp->link;
			index++;
		}
		return -1; //not found
	}

	int List::getSize() const
	{
		return size;
	}

}
