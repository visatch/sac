#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <ostream>

namespace listVisa
{
	using namespace std;
	const int MAX_LIST_SIZE = 50;
	struct Node
	{
		double data;
		Node* link;
	};
	typedef Node* NodePtr;

	class List
	{
	private:
		NodePtr head;
		int size;
	public:
		//Precondition: None
		//Postcondition: Default constructor
		List();
		//Precondition: None 
		//Postcondition: Destructor
		~List();
		//Precondition: Double item
		//Postcondition: Add an item to the list
		void addItem(double item);
		//Precondition: None
		//Postcondition: Return true if the linkedlist is full (max = 50), false otherwise
		bool full();
		//Precondition: None
		//Postcondition: Return current size of linkedlist
		int getSize() const;
		//Precondition: ostream Out, List list 
		//Postcondition: Output the list to the console
		friend ostream& operator <<(ostream& out, const List& list);
		//Precondition: The list is not empty
		//Postcondition: Return the last item on list
		double getLast();
		//Precondition: The list is not empty
		//Postcondition: Remove the last item on the list, size = size-1
		void removeLast();
		//Precondition: integer position, list is not empty
		//Postcondition: Return current data of node at position-th | -1 if false
		double getDataAtPos(int pos);
	};
}

#endif

