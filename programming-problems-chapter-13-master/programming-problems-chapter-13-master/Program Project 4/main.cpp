// Name: Visa Touch
// Date: 10-20-2020
// Description: Chapter13 - Programming Project 4

#include <iostream>
#include "input.h"
#include "List.h"
#include "time.h"
int main()
{
	cout << "Programming Project 4\n";
	using namespace listVisa;

	List list;
	srand(static_cast<unsigned int>(time(NULL)));
	cout << "\nAdd 5 items to the list.\n";
	for (int i = 0; i < 5; i++)
	{
		double tmp = ((rand() % 100) + 11.0) * 0.1;
		cout << "Add " << tmp << " to the list.\n";
		list.addItem(tmp);
	}
	cout << list;

	cout << "\nRemove last two items from the list\n";
	list.removeLast();
	list.removeLast();

	cout << "\nOutput the data at position 3:\n";
	cout << list.getDataAtPos(3) << '\n';


	
	



	return 0;
}


