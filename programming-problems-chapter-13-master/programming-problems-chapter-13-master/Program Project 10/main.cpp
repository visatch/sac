// Name: Visa Touch
// Date: 10-20-2020
// Description: Chapter13 - Programming Project 10

#include <iostream>
#include "input.h"
#include "Stack.h"

int main()
{
	cout << "Programming Project 10\n";
	using namespace stackVisa;
	Stack stack;
	string input;
	do
	{
		input = inputString("\nInput (0-9,+,-,*,/,q): ", false);
		if (isdigit(input.at(0)))
		{
			int tmp = stoi(input);
			stack.push(tmp);
		}
		else if (input.at(0) == 'q' || input.at(0) == 'Q')
			break;
		else
		{
			char sign = input.at(0);
			int tmp1 = 0, tmp2 = 0;
			if (!stack.empty())
				tmp1 = stack.pop();
			if (!stack.empty())
				tmp2 = stack.pop();
			switch(sign)
			{
			case '+':
				stack.push(tmp1 + tmp2);
				break;
			case '-':
				stack.push(tmp2 - tmp1);
				break;
			case '*':
				stack.push(tmp1 * tmp2);
				break;
			case '/':
				stack.push(tmp2 / tmp1);
				break;
			}
		}
	} while (true);
	
	if (!stack.empty())
		cout << "\nThe top of the stack is: " << stack.getData() << "\n";

	return 0;
}


