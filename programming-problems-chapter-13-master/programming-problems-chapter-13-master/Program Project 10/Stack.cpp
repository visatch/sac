#include "Stack.h"

namespace stackVisa 
{
	using namespace std;
	
	Stack::Stack() : top(NULL) {};
	Stack::Stack(const Stack& aStack)
	{
		if (aStack.top == NULL)
			top = NULL;
		else
		{
			StackFramePtr tmp = aStack.top;
			StackFramePtr end;
			end = new StackFrame;
			end->data = tmp->data;
			top = end;
			tmp = tmp->link;
			while (tmp != NULL)
			{
				end->link = new StackFrame;
				end = end->link;
				end->data = tmp->data;
				tmp = tmp->link;
			}
			end->link = NULL;
		}
	}
	
	Stack::~Stack()
	{
		int next;
		while (!empty())
			next = pop();
	}
	
	void Stack::push(int item)
	{
		StackFramePtr tmpPtr;
		tmpPtr = new StackFrame;
		tmpPtr->data = item;
		tmpPtr->link = top;
		top = tmpPtr;
	}

	int Stack::pop()
	{
		if (empty())
		{
			cout << "Error: popping an empty stack.\n";
			return -1;
		}
		int result = top->data;
		
		StackFramePtr tmpPtr;
		tmpPtr = top;
		top = top->link;
		delete tmpPtr;
		return result;
	}
	
	bool Stack::empty() const
	{
		return (top == NULL);
	}
	int Stack::getData()
	{
		if (!empty())
			return top->data;
		return -1;
	}
}