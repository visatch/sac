#ifndef STACK_H
#define STACK_H

#include <iostream>

namespace stackVisa
{
	using namespace std;

	struct StackFrame
	{
		int data;
		StackFrame* link;
	};
	typedef StackFrame* StackFramePtr;
	class Stack
	{
	private:
		StackFramePtr top;
	public:
		//Precondition: None
		//Postcondition: Create default object with link of NULL
		Stack();
		//Precondition: Stack& aStack
		//Postcondition: Create a new object from another object of Stack
		Stack(const Stack& aStack);
		//Precondition: None
		//Postcondition: Delete all nodes (Destructor)
		~Stack();
		//Precondition: int item
		//Postcondition: add an item to the Stack (top)
		void push(int item);
		//Precondition: The stack is not empty
		//Postcondition: Remove the top of the stack and return that item
		int pop();
		//Precondition: None
		//Postcondition: Return true if empty, false otherwise
		bool empty() const;
		//Precondition: The stack is not empty
		//Postcondition: Return the data of current StackFrame 
		int getData();
	};
}
#endif
