// Name: Visa Touch
// Date: 10-20-2020
// Description: Chapter13 - Programming Project 11

#include <iostream>
#include "input.h"
#include "Stack.h"
#include "Queue.h"

int main()
{
	cout << "Programming Project 11\n\n";
	using namespace stackVisa;
	using namespace queueVisa;
	Stack stack;
	Queue queue;

	string input;
	do
	{
		input = inputString("Input (0-9,+,-,*,/,q): ", false);
		char inputCharacter = input.at(0);
		if (input.at(0) == 'q' || input.at(0) == 'Q')
			break;
		else if (isdigit(inputCharacter))
			queue.add(input);
		else if (inputCharacter == '+' || inputCharacter == '-' || inputCharacter == '*' || inputCharacter == '/')
		{
			string tmp(1,inputCharacter);
			//cout << tmp;
			stack.push(tmp);
		}
		else if (inputCharacter == ')')
		{
			string tmp = stack.pop();
			queue.add(tmp);
		}
	} while (true);

	Stack newStack;
	while (!queue.empty())
	{
		string tmpString = queue.remove();
		if (isdigit(tmpString.at(0)))
			newStack.push(tmpString);
		else if (tmpString.at(0) == 'q' || tmpString.at(0) == 'Q')
			break;
		else
		{
			char sign = tmpString.at(0);
			string tmp1, tmp2, tmpStack;
			if (!newStack.empty())
				tmp1 = newStack.pop();
			if (!newStack.empty())
				tmp2 = newStack.pop();
			switch (sign)
			{
			case '+':
				tmpStack = to_string(stoi(tmp1) + stoi(tmp2));
				newStack.push(tmpStack);
				break;
			case '-':
				newStack.push(to_string(stoi(tmp2) - stoi(tmp1)));
				break;
			case '*':
				newStack.push(to_string(stoi(tmp1) * stoi(tmp2)));
				break;
			case '/':
				newStack.push(to_string(stoi(tmp2) / stoi(tmp1)));
				break;
			}
		}
	}

	if (!newStack.empty())
		cout << "\nThe top of the stack is: " << newStack.getData() << "\n";

	return 0;
}


