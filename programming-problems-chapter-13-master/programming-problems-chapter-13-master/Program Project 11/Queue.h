#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <string>

namespace queueVisa
{
	using namespace std;
	struct QueueNode
	{
		string data;
		QueueNode* link;
	};

	typedef QueueNode* QueueNodePtr;

	class Queue
	{
	private:
		QueueNodePtr front;
		QueueNodePtr back;
	public:
		//Precondition: None
		//Postcondition: Create a default object
		Queue();
		//Precondition: Queue& = queue
		//Postcondition: Create a queue from another queue 
		Queue(const Queue& queue);
		//Destructor
		~Queue();
		//Precondition: char item
		//Postcondition: add an item to back of the queue
		void add(string item);
		//Precondition: The queue is not empty
		//Postcondition: Return the item at the front of the queue and removes that item from the queue
		string remove();
		//Precondition: None
		//Postcondition: Return ture if the queue is empty, false otherwise
		bool empty() const;
	};
}
#endif


