#include "Queue.h"
#include <iostream>

namespace queueVisa
{
	using namespace std;
	Queue::Queue() :front(NULL), back(NULL) {};

	Queue::Queue(const Queue& queue)
	{
		if (queue.empty())
			front = back = NULL;
		else
		{
			QueueNodePtr tmpPtrOld = queue.front;
			QueueNodePtr tmpPtrNew;
			back = new QueueNode;
			back->data = tmpPtrOld->data;
			back->link = NULL;
			front = back;
			tmpPtrOld = tmpPtrOld->link;
			while (tmpPtrOld != NULL)
			{
				tmpPtrNew = new QueueNode;
				tmpPtrNew->data = tmpPtrOld->data;
				tmpPtrNew->link = NULL;
				back->link = tmpPtrNew;
				back = tmpPtrNew;
				tmpPtrOld = tmpPtrNew->link;
			}
		}
	};

	Queue:: ~Queue()
	{
		string tmp;
		while (!empty())
			tmp = remove();
	}

	void Queue::add(string item)
	{
		if (empty())
		{
			front = new QueueNode;
			front->data = item;
			front->link = NULL;
			back = front;
		}
		else
		{
			QueueNodePtr tmpPtr;
			tmpPtr = new QueueNode;
			tmpPtr->data = item;
			tmpPtr->link = NULL;
			back->link = tmpPtr;
			back = tmpPtr;
		}
	}

	string Queue::remove()
	{
		if (empty())
		{
			cout << "Error: Removing an item from an empty queue.\n";
			return "";
		}
		string result = front->data;
		QueueNodePtr discard;
		discard = front;
		front = front->link;
		if (front == NULL)
			back = NULL;

		delete discard;
		return result;
	}

	bool Queue::empty() const
	{
		return (back == NULL);
	}
}